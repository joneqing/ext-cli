'use strict';

const semver = require("semver");
const colors = require('colors/safe')
const log = require('@ext-cli/log')

const LOWEST_NODE_VERSION = '12.0.0'

class Command {
  constructor(argv) {
    if (!argv) {
      throw new Error('参数不能为空!')
    }
    if (!Array.isArray(argv)) {
      throw new Error('参数必须为数组!')
    }
    if (argv.length < 1) {
      throw new Error('参数列表为空, 请检查!')
    }
    this._argv = argv
    let runner = new Promise((resolve, reject) => {
      let chain = Promise.resolve()
      chain = chain.then(() => this.checkNodeVersion())
      chain = chain.then(() => this.initArgs())
      chain = chain.then(() => this.init())
      chain = chain.then(() => this.exec())
      chain.catch(err => {
        log.error(err.message)
      })
    })
  }

  // 初始化参数
  initArgs() {
    this._cmd = this._argv[this._argv.length - 1]
    this._argv = this._argv.slice(0, this._argv.length - 1)
  }

  // 检查node版本
  checkNodeVersion() {
    // 获取当前Node版本号
    const currentVersion = process.version
    const lowestVersion =  LOWEST_NODE_VERSION
    // 比对最低版本号
    if (!semver.gte(currentVersion, lowestVersion)) {
      throw new Error(colors.red(`ext-cli 需要安装 v${lowestVersion} 以上版本的Node.js`))
    }
  }

  init() {
    throw new Error('未实现init方法')
  }

  exec() {
    throw new Error('未实现exec方法')
  }
}

module.exports = Command;