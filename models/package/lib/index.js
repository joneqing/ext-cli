'use strict';

const path = require('path')
const {isObject} = require('@ext-cli/utils')
const formatPath = require('@ext-cli/format-path')
const npmInstall = require('npminstall')
const {getDefaultRegistry, getNpmLatestVersion} = require('@ext-cli/get-npm-info')
const pathExists = require('path-exists').sync
const pkgDir = require('pkg-dir').sync
const fsExtra = require('fs-extra')

class Package {
  constructor(options) {
    if (!options) {
      throw new Error('Package类的options不能为空')
    }
    if (!isObject(options)) {
      throw new Error('Package类的options参数必须为对象')
    }
    // package的目标路径
    this.targetPath = options.targetPath
    // package的缓存路径
    this.storeDir = options.storeDir
    this.packageName = options.packageName
    this.packageVersion = options.packageVersion
    // package的缓存目录前缀
    this.cacheFilePathPrefix = this.packageName.replace('/', '_')
  }

  async prepare() {
    if (this.storeDir && !pathExists(this.storeDir)) {
      fsExtra.mkdirpSync(this.storeDir) // 创建缓存目录
    }
    if (this.packageVersion === 'latest') {
      this.packageVersion = await getNpmLatestVersion(this.packageName) // 获取最新版本号
    }
  }

  get cacheFilePath() {
    return path.resolve(this.storeDir, `_${this.cacheFilePathPrefix}@${this.packageVersion}@${this.packageName}`)
  }

  // 获取指定版本号文件路径
  getSpecificCacheFilePath(packageVersion) {
    return path.resolve(this.storeDir, `_${this.cacheFilePathPrefix}@${packageVersion}@${this.packageName}`)
  }

  // 判断当前package是否存在
  async exists() {
    if (this.storeDir) {
      await this.prepare()
      return pathExists(this.cacheFilePath)
    } else {
      return pathExists(this.targetPath)
    }
  }

  // 安装package
  async install() {
    await this.prepare()
     return npmInstall({
      root: this.targetPath,
      storeDir: this.storeDir,
      registry: getDefaultRegistry(),
      pkgs: [
        {name: this.packageName, version: this.packageVersion}
      ]
    })
  }

  // 更新package
  async update() {
    await this.prepare()
    // 获取最新的npm模块版本号
    const latestPackageVersion = await getNpmLatestVersion(this.packageName)
    // 查询最新版本号对应的路径是否存在
    const latestFilePath = this.getSpecificCacheFilePath(latestPackageVersion)
    // 如果不存在， 安装最新版本
    if (!pathExists(latestFilePath)) {
      await npmInstall({
        root: this.targetPath,
        storeDir: this.storeDir,
        registry: getDefaultRegistry(),
        pkg: [
          {name: this.packageName, version: latestFilePath}
        ]
      })
      this.packageVersion = latestPackageVersion
    } else {
      this.packageVersion = latestPackageVersion
    }
  }

  // 获取入口文件的路径
  getRootFilePath() {
    function _getRootFile(targetPath) {
      // 获取package.json所在目录
      const dir = pkgDir(targetPath)
      // 读取package.json
      if (dir) {
        const pkgFile = require(path.resolve(dir, 'package.json'))
        // 寻找main/lib
        if (pkgFile && pkgFile.main) {
          // 路径兼容(macos/windows)
          return formatPath(path.resolve(dir, pkgFile.main))
        }
      }
      return null
    }
    if (this.storeDir) {
      return _getRootFile(this.cacheFilePath)
    } else {
      return _getRootFile(this.targetPath)
    }
  }
}

module.exports = Package
