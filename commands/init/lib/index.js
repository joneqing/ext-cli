'use strict';

const Command = require('@ext-cli/command')
const log = require('@ext-cli/log')
const fs = require('fs')
const inquirer = require('inquirer')
const fsExtra = require('fs-extra')
const semver = require('semver')
const ejs = require('ejs')
const glob = require('glob')
const Package = require('@ext-cli/package')
const userHome = require('user-home')
const path = require('path')
const kebabCase = require('kebab-case')
const { spinnerStart, sleep, execAsync } = require('@ext-cli/utils')

const getProjectTemplate = require('./getProjectTemplate')

const TYPE_PROJECT = 'project'
const TYPE_COMPONENT = 'component'

const TEMPLATE_TYPE_NORMAL = 'normal'
const TEMPLATE_TYPE_CUSTOM = 'custom'

// 执行命令白名单
const WHITE_COMMAND = ['npm', 'cnpm', 'yarn', 'pnpm']

class InitCommand extends Command {
  init() {
    this.projectName = this._argv[0] || ''
    this.force = !!process.env.CLI_INIT_FORCE
    log.verbose('projectName', this.projectName)
    log.verbose('force', this.force)
  }

  async exec() {
    try {
      // 准备阶段
      const projectInfo = await this.prepare()
      if (projectInfo) {
        log.verbose('projectInfo', projectInfo)
        this.projectInfo = projectInfo
        // 下载模板
        await this.downloadTemplate()
        // 安装模板
        await this.installTemplate()
      }
    } catch(e) {
      log.error(e.message)
      if (process.env.LOG_LEVEL === 'verbose') {
        console.log(e)
      }
    }
  }

  async prepare() {
    // 判断项目模板是否存在
    const template = await getProjectTemplate()
    if (!template || template.length === 0) {
      throw new Error('项目模板不存在')
    }
    this.template = template
    // 判断当前目录是否为空
    const localPath = process.cwd()
    const ret = this.isDirEmpty(localPath)
    if (ret) {

    } else {
      let ifContinue = false
      if (!this.force) {
        // 询问是否继续创建
        ifContinue = (await inquirer.prompt({
          type: 'confirm',
          name: 'ifContinue',
          default: false,
          message: '当前文件夹不为空，是否继续创建项目'
        })).ifContinue
      }
      // 是否启动强制更新
      if(ifContinue || this.force) {
        // 二次确认删除
        const { confirmDelete } = await inquirer.prompt({
          type: 'confirm',
          name: 'confirmDelete',
          default: false,
          message: '是否确认清空当前目录下的文件?'
        })
        if (confirmDelete) {
          // 清空当前目录
          fsExtra.emptyDirSync(localPath)
        }
      }
    }

    return this.getProjectInfo()
  }

  async getProjectInfo() {
    function isValidName(v) {
      return /^$[a-zA-Z]+([-][a-zA-Z]+[a-zA-Z0-9]*[_][a-zA-Z][a-zA-Z0-9]*|[a-zA-Z0-9])$/.test(v)
    }
    let projectInfo = {}
    let isProjectNameValid = false
    if (isValidName(this.projectName)) {
      isProjectNameValid = true
      projectInfo.projectName = this.projectName
    }

    // 选择创建项目或组件
    const { type } = await inquirer.prompt({
      type: 'list',
      name: 'type',
      message: '请选择初始化类型',
      default: TYPE_PROJECT,
      choices: [{
        name: '项目',
        value: TYPE_PROJECT
      }, {
        name: '组件',
        value: TYPE_COMPONENT
      }]
    })
    log.verbose('choice type: ', type)

    if (type === TYPE_PROJECT) {
      // 获取项目基本信息
      const projectNamePrompt = {
        type: 'input',
        name: 'projectName',
        message: '请输入项目名称',
        default: '',
        validate: v => {
          const done = this.async()
          setTimeout(() => {
            if (!isValidName(v)) {
              done('请输入合法的项目名称(首字符为英文字符,尾字符为英文或数字，不能为其他字符, 字符仅允许-和_)')

            }
          }, 0)
          done(null, true)
        },
      }
      const projectPrompt = []
      if (!isProjectNameValid) {
        projectPrompt.push(projectNamePrompt)
      }
      projectPrompt.push({
        type: 'input',
        name: 'projectVersion',
        message: '请输入项目版本号',
        default: '',
        validate: v => {
          const done = this.async()
          setTimeout(() => {
            if (!!semver.validate(v)) {
              done('请输入合法的版本号')
            }
            done(null, true)
          }, 0)
        },
        filter: v => {
          if (!!semver.validate(v)) {
            return semver.validate(v)
          } else {
            return v
          }
        },
      })
      projectPrompt.push({
        type: 'list',
        name: 'projectTemplate',
        message: '请选择项目模板',
        choices: this.createPrjectTemplate()
      })
      const project = await inquirer.prompt(projectPrompt)
      projectInfo = {
        ...projectInfo,
        type,
         ...project
      }
    } else if (type === TYPE_COMPONENT) {

    }
    // 生成classname
    if (projectInfo.projectName) {
      projectInfo.name = projectInfo.projectName
      projectInfo.className = kebabCase(projectInfo.projectName).replace(/^-/, '')
    }
    if (projectInfo.projectVersion) {
      projectInfo.version = projectInfo.projectVersion
    }
    return projectInfo
  }

  createPrjectTemplate() {
    return this.template.map(item => ({
      value: item.pkgName,
      name: item.pkgDesc
    }))
  }

  // 判断当前目录是否为空
  isDirEmpty(localPath) {
    let fileList = fs.readdirSync(localPath)
    // 文件过滤逻辑
    fileList = fileList.filter(file => (
      !file.startsWith('.') && ['node_modules'].indexOf(file) < 0
    ))
    return !fileList || fileList.length <= 0
  }

  async downloadTemplate() {
    const { projectTemplate } = this.projectInfo
    const templateInfo = this.template.find(item => item.pkgName === projectTemplate)
    const targetPath = path.resolve(userHome, '.ext-cli', 'template')
    const storeDir = path.resolve(userHome, '.ext-cli', 'template', 'node_modules')
    const { pkgName, pkgVersion } = templateInfo
    this.templateInfo = templateInfo
    const templateNpm = new Package({
      targetPath,
      storeDir,
      packageName: pkgName,
      packageVersion: pkgVersion
    })
    log.verbose('templateNpm: ', templateNpm)
    if (!await templateNpm.exists()) {
      const spinner = spinnerStart('模板下载中...')
      await sleep()
      try {
        await templateNpm.install()
      } catch(err) {
        throw err
      } finally {
        spinner.stop(true)
        if (await templateNpm.exists()) {
          log.success('模板下载成功')
          this.templateNpm = templateNpm
        }
      }
    } else {
      const spinner = spinnerStart('模板更新中...')
      await sleep()
      try {
        await templateNpm.update()
      } catch(err) {
        throw err
      } finally {
        spinner.stop(true)
        if (await templateNpm.exists()) {
          log.success('模板更新成功')
          this.templateNpm = templateNpm
        }
      }
    }
  }

  async templateRender(options) {
    const dir = process.cwd()
    const templateInfo = this.templateInfo
    return new Promise((resolve, reject) => {
      glob('**', {
        cwd: dir,
        ignore: options.ignore || '',
        nodir: true, // 忽略文件夹
      }, (err, files) => {
        if (err) {
          reject(err)
        }
        Promise.all(files.map(file => {
          const filePath = path.join(dir, file)
          return new Promise((resolve1, reject1) => {
            ejs.renderFile(filePath, templateInfo, {}, (err, result) => {
              if (err) {
                reject1(err)
              } else {
                fsExtra.writeFileSync(filePath, result)
                resolve1(result)
              }
            })
          })
        })).then(() => {
          resolve()
        }).catch(err => {
          reject(err)
        })
      })
    })
  }

  async installTemplate() {
    log.verbose('templateInfo: ', this.templateInfo)
    const templateInfo = this.templateInfo
    if (templateInfo) {
      if (!templateInfo.type) {
        templateInfo.type = TEMPLATE_TYPE_NORMAL
      }
      if (templateInfo.type === TEMPLATE_TYPE_NORMAL) {
        // 标准安装
        await this.installNormalTemplate()
      } else if(templateInfo.type === TEMPLATE_TYPE_CUSTOM) {
        // 自定义安装
        await this.installCustomTemplate()
      } else {
        throw new Error('无法识别项目模板类型')
      }
    } else {
      throw new Error('项目模板信息不存在')
    }
  }

  checkCommand(cmd) {
    if (WHITE_COMMAND.includes(cmd)) {
      return cmd
    }
    return null
  }

  async execCommand(command, errMsg) {
    let ret
    if (command ) {
      const cmdArr = command.split(' ')
      const cmd = this.checkCommand(cmdArr[0])
      if (!cmd) {
        throw new Error('命令: ' + cmd + '不存在!')
      }
      const args = cmdArr.slice(1)
      ret = await execAsync(cmd, args, {
        stdio: 'inherit',
        cwd: process.cwd()
      })
      if (ret !== 0) {
        throw new Error(errMsg)
      }
      return ret
    }
  }

  async installNormalTemplate() {
    // 拷贝模板代码至当前目录
    let spinner = spinnerStart('正在安装模板...')
    await sleep()
    try {
      const templatePath = path.resolve(this.templateNpm.cacheFilePath, 'template')
      const targetPath = process.cwd()
      fsExtra.ensureDirSync(templatePath)
      fsExtra.ensureDirSync(targetPath)
      fsExtra.copySync(templatePath, targetPath)
    } catch(e) {
      throw e
    } finally {
      spinner.stop(true)
      log.success('模板安装成功')
    }
    const ignore = ['node_modules/**', 'public/**']
    await this.templateRender({ignore})
    // 依赖安装
    const { installCmd, startCmd } = this.templateInfo
    await this.execCommand(installCmd, '依赖安装失败!')
    // 启动命令执行
    await this.execCommand(startCmd, '启动执行命令失败!')
  }

  async installCustomTemplate() {

  }
}

const init = (argv) => {
  return new InitCommand(argv)
}

module.exports = init;
module.exports.InitCommand = InitCommand
