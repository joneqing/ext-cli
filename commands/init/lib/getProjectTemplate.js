const request = require('@ext-cli/request');

const getProjectTemplate = () => {
  return request({
    url: '/project/template'
  })
}

module.exports = getProjectTemplate
