'use strict';

const axios = require('axios')
const urlJoin = require('url-join')
const semver = require('semver')

const getNpmInfo = (moduleName, registry) => {
  if (!moduleName) return;
  const registryUrl = registry || getDefaultRegistry()
  const npmInfoUrl = urlJoin(registryUrl, moduleName)

  return axios.get(npmInfoUrl).then(res => {
    if (res.status === 200) {
      return res.data
    }
    return null
  }).catch(err => {
    return Promise.reject(err)
  })
}

const getModuleVersions = async (moduleName, registry) => {
  const data = await getNpmInfo(moduleName, registry)
  if (data) {
    return Object.keys(data.versions)
  } else {
    return []
  }
}

const getSemverVersions = (baseVersion, versions) => {
  return versions.filter(version => semver.satisfies(version, `>${baseVersion}`)).sort((a, b) => semver.gt(b, a) )
}

const getModuleSemverVersions = async (baseVersion, moduleName, registry) => {
  const versions = await getModuleVersions(moduleName, registry)
  const newVersions = getSemverVersions(baseVersion, versions)
  if (newVersions && newVersions.length > 0) {
    return newVersions[0]
  }
}

const getDefaultRegistry = (isOrigin = true) => {
  return isOrigin ? 'https://registry.npmjs.org' : 'https://registry.npmmirror.com'
}

const getNpmLatestVersion = async (moduleName, registry) => {
  let versions = await getModuleVersions(moduleName, registry)
  if (versions && versions.length > 0) {
    return versions.sort((a, b) =>  semver.gt(b, a))[0]
  }
  return null
}

module.exports = {
  getNpmInfo,
  getModuleVersions,
  getSemverVersions,
  getModuleSemverVersions,
  getDefaultRegistry,
  getNpmLatestVersion,
}