# ext-cli
### 前端脚手架工具框架
  技术栈: Lerna + Nodejs + Koa

### How To Use
```shell
npm install -g @ext-cli/core

ext init [options] [projectName]
或
ext init -h
```
!注意使用npm官方镜像地址
