'use strict';

module.exports = core;

const path = require('path')
const semver = require('semver')
const colors = require('colors/safe')
const userHome = require('user-home')
const pathExists = require('path-exists')
const log  = require('@ext-cli/log')
const exec = require('@ext-cli/exec')
const { Command } = require('commander')
const program = new Command()
const pkg = require('../package.json')
const constant = require('./constant')

async function core() {
  try {
    await prepare() // 脚手架准备
    registryCommand() // 脚手架注册
  } catch(e) {
    log.error(e.message)
    if (program.opts().debug) {
      console.error(e)
    }
  }
}

const prepare = async () => {
  checkPkgVersion() // 检查版本号
  checkRoot() // 检查root启动
  checkUserHome() // 检查用户主目录
  checkEnv() // 检查环境变量
  await checkGlobalUpdate() // 检查版本更新
}

const registryCommand = () => {
  program
    .name(Object.keys(pkg.bin)[0])
    .usage('<command> [options]')
    .version(pkg.version)
    .option('-d, --debug', '是否开启调试模式', false)
    .option('-tp, --targetPath <targetPath>', '是否指定本地调试文件路径', '')

  program
    .command('init [projectName]')
    .option('-f, --force', '是否强制初始化')
    .action(exec)

  const options = program.opts()

  // 开启debug模式
  program.on('option:debug', () => {
    if (options.debug) {
      process.env.LOG_LEVEL = 'verbose'
    } else {
      process.env.LOG_LEVEL = 'info'
    }
    log.level = process.env.LOG_LEVEL
  })

  // 指定targetPath
  program.on('option:targetPath', () => {
    if (options.targetPath) {
      process.env.CLI_TARGET_PATH = options.targetPath
    }
  })

  // 对未知命令监听
  program.on('command:*', obj => {
    // const availableCommands = program.commands.map(cmd => cmd.name())
    console.log(colors.red('未知命令: ' + obj[0]))
  })

  program.parse(process.argv)

  if (program.args && program.args.length < 1) {
    program.outputHelp()
    console.log()
  }
}

const checkGlobalUpdate = async () => {
  // 获取当前版本号和模块名
  const currentVersion = pkg.version
  const moduleName = pkg.name
  // 调用npm api 获取所有版本号
  const { getModuleSemverVersions } = require('@ext-cli/get-npm-info')
  const lastVersion = await getModuleSemverVersions(currentVersion, moduleName)
  if (lastVersion && semver.gt(lastVersion, currentVersion)) {
    log.warn(colors.yellow(`请手动更新 ${moduleName}, 当前版本: ${currentVersion}, 最新版本: ${lastVersion}
         更新命令: npm install -g ${moduleName}`))
  }
}

const checkEnv = () => {
  const dotenv = require('dotenv')
  const dotenvPath = path.resolve(userHome, '.env')
  if (pathExists(dotenvPath)) {
    dotenv.config({
      path: dotenvPath
    })
  }
  createDefaultConfig()
}

const createDefaultConfig = () => {
  const cliConfig = {
    home: userHome,
  }
  if (process.env.CLI_HOME) {
    cliConfig.cliHome = path.join(userHome, process.env.CLI_HOME)
  } else {
    cliConfig.cliHome = path.join(userHome, constant.DEFAULT_CLI_HOME)
  }
  process.env.CLI_HOME_PATH = cliConfig.cliHome
}

const checkUserHome = () => {
  if (!userHome) {
    throw new Error(colors.red(`当前登录用户主目录不存在!`))
  }
}

const checkRoot = () => {
  const rootCheck = require('root-check')
  rootCheck()
}

const checkPkgVersion = () => {
  log.notice('cli', pkg.version)
}
