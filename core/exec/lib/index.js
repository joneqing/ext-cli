'use strict';

const path = require('path')
const Package = require('@ext-cli/package')
const log = require('@ext-cli/log')
const { exec: spawn } = require('@ext-cli/utils')

const SETTINGS = {
  init: '@ext-cli/init'
}

const CACHE_DIR = 'dependencies'

async function exec() {
  let targetPath = process.env.CLI_TARGET_PATH
  log.verbose('targetPath', targetPath)
  let pkg
  const homePath = process.env.CLI_HOME_PATH
  log.verbose('homePath', homePath)
  const cmdObj = arguments[arguments.length - 1]
  const cmdName = cmdObj.name() // 获取命令名称
  const packageName = SETTINGS[cmdName]
  const packageVersion = 'latest'

  if (cmdObj.opts().force) {
    process.env.CLI_INIT_FORCE = cmdObj.opts().force
  }

  if (!targetPath) {
    // 生成缓存目录
    targetPath = path.resolve(homePath, CACHE_DIR)
    log.verbose('default targetPath', targetPath)
    const storeDir = path.resolve(targetPath, 'node_modules')
    log.verbose('storeDir', storeDir)
    pkg = new Package({
      targetPath,
      packageName,
      storeDir,
      packageVersion
    })
    if (await pkg.exists()) {
      // 更新package
      await pkg.update()
    } else {
      // 安装package
      await pkg.install()
    }
  } else {
    pkg = new Package({
      targetPath,
      packageName,
      packageVersion
    })
  }
  const rootFile = pkg.getRootFilePath()
  if (rootFile) {
    // 在当前进程中调用
    try {
      // 在node子进程中调用
      const args = Array.from(arguments)
      const cmd = args[args.length - 1]
      const o = Object.create(null)
      Object.keys(cmd).forEach(key => {
        if (!key.startsWith('_') && key !== 'parent') {
          o[key] = cmd[key]
        }
      })
      args[args.length - 1] = o
      const code = `require('${rootFile}').call(null, ${JSON.stringify(args)})`
      const child = spawn('node', ['-e', code], {
        cwd: process.cwd(),
        stdio: 'inherit'
      })
      child.on('error', e => {
        log.error(e.message)
        process.exit(1)
      })
      child.on('exit', e => {
        log.verbose('命令执行成功: ' + e)
        process.exit(0)
      })
    } catch(e) {
      log.error(e.message)
    }
  }
}

module.exports = exec
